FROM alpine:3.11
RUN apk add --no-cache py3-pip && \
    pip3 --no-cache-dir install mkdocs mkdocs-material mkdocs-macros-plugin mkdocs-redirects && \
    rm -rf /root/.cache