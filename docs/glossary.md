# Glossary of common terms

## RCW (Reset Configuration Word)
This is a set bytes that is read by the LS1088 CPU at startup, to configure
clocks, active hardware blocks and pin multiplexing options. You can see a
read out from u-boot:
```
Reset Configuration Word (RCW):
       00000000: 4000541c 00000040 00000000 00000000
       00000010: 00000000 000a0000 00300000 00000000
       00000020: 010011a0 00002580 00000000 00000000
       00000030: 013fe60b 00000000 00002c03 00000000
       00000040: 00000000 00000000 00000000 00000000
       00000050: 00000000 00000000 00000000 00000000
       00000060: 00000000 00000000 00000089 000009e7
       00000070: 44110000 0d007755

```

A set of pins denoted cfg_rcw_src\[0:8] decide which media the CPU reads the RCW from -
the board microcontroller will drive these pins for the desired boot option (QSPI or SD)

## Management Complex
Management Complex provides management services for the DPAA2 resources. It provides a 'bus' style
interface to expose resources under it (such as DPNI's and DPIO's), as well as a firmware interface
to receive commands from the operating system.

## DPNI
Data Path Network Interface - the DPAA2 object corresponding to a network interface.

## DPMAC
Data Path MAC - the DPAA2 object corresponding to an Ethernet MAC.

Typically, DPMAC and DPNI are wired together.

## DPIO,DPCON,DPBP,DPRC, DPMCP and other DPAA2 objects
see [DPAA2 overview](/network/dpaa2overview)

## QSPI
Quad SPI - an enhanced SPI (Serial Peripheral Interface) that improves bandwidth
by using more lanes for transfer. The boot [flash](/hardware/flash) memory devices are connected
to the SoC with QSPI.

## BL1/2/3
Stages in the Trusted Firmware boot loader, BL1 is the initial code executed by
the processor after reset, BL2 loads and authenticates further images such as BL31/BL32/BL33.

See the [Trusted Firmware](https://trustedfirmware-a.readthedocs.io/en/latest/getting_started/image-terminology.html)
documentation for more details.
