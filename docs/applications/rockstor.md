# Rockstor on Ten64 HOW-TO and notes

[Rockstor](http://rockstor.com/) 4.0 and later works on the Ten64 both as
a native (bare-metal) OS and as a virtual machine.

At the time of writing, the 4.0 branch (which is based on openSusE, instead of CentOS in previous versions) does not have any released binaries - you have to build an image yourself from the [kiwi-ng recipies](https://github.com/rockstor/rockstor-installer/). Ten64 beta testers can obtain an image by contacting Traverse.

Once Rockstor 4 is released we will list it in the appliance store for bare metal and VM deployment.

## Bare metal
Bare metal deployment is easy - write the generated qcow2 image using the [recovery](/software/recovery.md) `write-image-disk` command. 

On first boot, the [Rockstor setup program](http://rockstor.com/docs/installer-howto/installer-howto.html) will take you through the steps of setting up your root password and other details, before setting up Web-UI access.

## VM under μVirt
The main issue to be mindful of when running Rockstor as a VM is that Rockstor requires serial numbers to be assigned for every block device presented to it.

In LuCI, you can assign a device serial to every device like so:

![Rockstor device serial](/images/rockstor-vm-config.png)

In UCI, you can assign a device serial by appending ",serial=XXXX" to the disk option:
```
config vm 'rockstor'
        option numprocs '2'
        option memory '3072'
        option mac '52:54:00:ab:cd:ef'
        list disks '/dev/mapper/vmdata-rockstor,serial=ROCKSTORROOT'
        list disks '/dev/sdb,serial=wd10ezex1'
        list disks '/dev/sdc,serial=wd10ezex2'
        list disks '/dev/sdd,serial=wd10ezex3'
        list disks '/dev/sde,serial=wd10ezex4'
        option enable '1'
        option provisioned '1'
        list network 'lan'
```

### Considerations
You can use Rockstor with just one disk (the install drive), however, it is better to use a separate drive for your data.

If you need to reinstall Rockstor in the future you can just recreate the system drive and then import your data drive.

### Passing through 'real' block devices into a VM
There are two ways you can pass through 'real' block devices, such as SATA drives.

* Using [VFIO](/software/vfio/), pass through the controller.

    This has the advantage that SMART and other low level functions will work inside Rockstor, plus this avoids the overhead on the host side translating block commands from the
    virtio interface to the Linux block interface.

    However, we have encountered some driver issues with certain controllers and VFIO in the past - at the time of writing (2020-10) it does seem to work ok with Rockstor 4.

* Pass through the block device only

    You will lose low level functions such as SMART but this method is very reliable. As described above, you will need to a assign a serial number to each device from your VM host.

    Make sure your host operating system does not attempt to mount your storage devices while the Rockstor VM is in operation.

### Performance Notes
When using a PCIe SATA controller card, you may be able to improve performance (especially disk<->network) by reassigning the SATA/AHCI interrupt to another CPU core.

Locate the AHCI irq in `/proc/interrupts`:
```
rockstortest:~ # grep ahci /proc/interrupts 
 53:        660          0   ITS-MSI 98304 Edge      ahci[0000:00:06.0]
```

To move SATA interrupt processing to CPU core 1, you can do:
```
echo 1 > /proc/irq/53/smp_affinity_list
```

Then do a disk operation to see if interrupts are arriving on the new core:
```
rockstortest:~ # dd if=/dev/sda bs=4096 count=65535 of=/dev/null
65535+0 records in
65535+0 records out
268431360 bytes (268 MB, 256 MiB) copied, 0.66813 s, 402 MB/s

rockstortest:~ # grep ahci /proc/interrupts 
 53:        660        519   ITS-MSI 98304 Edge      ahci[0000:00:06.0]
```

### Rock-Ons
Rockstor's [Rock-Ons](http://rockstor.com/docs/docker-based-rock-ons/overview.html) are an excellent way of deploying common applications to your system, leveraging the storage provided by Rockstor and avoiding the hassles of manually creating containers.

Note that the Rock-On feature currently shows all available Rock-Ons in the registry, it will not prevent you from installing Rock-Ons which only work on x86. Please see [issue #2191](https://github.com/rockstor/rockstor-core/issues/2191) for more information.