# GPIOs

There are two different GPIO controllers that you might interact with on the Ten64 board:

* The SoC GPIO controller, mainly for the control header (has both GPIO and GIC IRQs) and some status LEDs.

    Note SoC GPIOs may have limitations on current source/sink, different min/max voltages depending on the voltage domain,
or lack ESD protection.

* The PCA/TCA9534 GPIO expander (U39), used for control IO/s in its vicinity (SFP, LTE),
as well as some with special requirements (HD activity LED).

See [Control Header](/hardware/control-header/) for how to access the named `GPIO_` pins.

## Table of GPIOs
| GPIO Controller | Pin | Linux GPIO number <br/>(>6.2, earlier)       | Default Direction | Purpose                                       | Meaning of ‘0’            | Meaning of ‘1            | Notes                                                                                                                    |
|-----------------|-----|----------------------------------------------|-------------------|-----------------------------------------------|---------------------------|--------------------------|--------------------------------------------------------------------------------------------------------------------------|
| TCA9539 (U39)   | 0   | 640 <small>368</small>                       | Input             | Lower SFP+ TXFAULT                            | No transmitter fault      | Transmitter Fault        | Has pull up. When using SFP managed mode, the Phylink system will control these GPIOs                                    |
| TCA9539 (U39)   | 1   | 641 <small>369</small>                       | Output            | Lower SFP+ TXDISABLE                          | SFP+ transmitter disabled | SFP+ transmitter enabled | Has pull up – if the pin is not driven as output it goes to 1, this pin may not be effective with direct connect (DAC) cables |
| TCA9539 (U39)   | 2   | 642 <small>370</small>                       | Input             | Lower SFP+ PRESENT                            | Lower SFP present         | Lower SFP not present    |                                                                                                                          |
| TCA9539 (U39)   | 3   | 643 <small>371</small>                       | Input             | Lower SFP+ LOS                                | Lower SFP lost signal     | Lower SFP has signal     |                                                                                                                          |
| TCA9539 (U39)   | 4   | 644 <small>372</small>                       | Input             | Upper SFP+ TXFAULT                            | No transmitter fault      | Transmitter Fault        | Has pull up                                                                                                              |
| TCA9539 (U39)   | 5   | 645 <small>373</small>                       | Output            | Upper SFP+ TXDISABLE                          | SFP+ transmitter disabled | SFP+ transmitter enabled | Has pull up – if the pin is not driven as output it goes to 1, this pin may not be effective with direct connect (DAC) cables |
| TCA9539 (U39)   | 6   | 646 <small>374</small>                       | Input             | Upper SFP+ PRESENT                            | Upper SFP present         | Upper SFP not present    |                                                                                                                          |
| TCA9539 (U39)   | 7   | 647 <small>375</small>                       | Input             | Upper SFP+ LOS                                | Upper SFP lost signal     | Upper SFP has signal     |                                                                                                                          |
| TCA9539 (U39)   | 8   | 648 <small>376</small>                       | Output            | Cellular Modem (M.2/B) RESET                  |                           |                          |                                                                                                                          |
| TCA9539 (U39)   | 9   | 649 <small>377</small>                       | Output            | Cellular Modem (M.2/B) POWER OFF              |                           |                          |                                                                                                                          |
| TCA9539 (U39)   | 10  | 650 <small>378</small>                       | Output            | Cellular Modem (M.2/B) DISABLE                |                           |                          |                                                                                                                          |
| TCA9539 (U39)   | 11  | 651 <small>379</small>                       | Output            | Cellular Modem (M.2/B) GNSS                   |                           |                          |                                                                                                                          |
| TCA9539 (U39)   | 12  | 652 <small>380</small>                       | Output            | I/O Panel "ADMIN" LED+                        | LED off                   | LED on                   | Both 380 and 381 need to be set up as outputs. On some beta test boards the ADMIN LED is bi-color.                       |
| TCA9539 (U39)   | 13  | 653 <small>381</small>                       | Output            | I/O Panel "ADMIN" LED-                        | Must be 0 for LED to function |                      | Set as a "GPIO Hog" in the Ten64 device tree                                                                             |
| TCA9539 (U39)   | 14  | 654 <small>382</small>                       | Output            | “User Switch” / P9 Header                     | P9 is pressed             | P9 not pressed           |                                                                                                                          |
| TCA9539 (U39)   | 15  | 655 <small>383</small>                       | Output            | ATX Front Panel Hard Drive Activity LED       |                           |                          |                                                                                                                          |
| LS1088 GPIO2    | 17  | 561 <small>465</small>                       | Input             | ATX Power Down / Control Header PWR_BTN_N     | Request Shutdown          | Normal Function          | When gpio-keys driver enabled, kernel will listen on this pin                                                            |
| LS1088 GPIO3    | 27  | 635 <small>443</small>                       | Input             | GPIO Header GPIO_A                            |                           |                          |
| LS1088 GPIO3    | 28  | 636 <small>444</small>                       | Input             | GPIO Header GPIO_B                            |                           |                          |
| LS1088 GPIO3    | 29  | 637 <small>445</small>                       | Input             | GPIO Header GPIO_C                            |                           |                          |                                                                                                                          |
| LS1088 GPIO4    | 4   | 612 <small>388</small>                       | Input             | GPIO Header GPIO_D                            |                           |                          |                                                                                                                          |
| LS1088 GPIO4    | 5   | 613 <small>389</small>                       | Input             | GPIO Header GPIO_E                            |                           |                          |                                                                                                                          |
| LS1088 GPIO4    | 6   | 614 <small>390</small>                       | Input             | GPIO Header GPIO_F                            |                           |                          |                                                                                                                          |
| LS1088 GPIO4    | 7   | 615 <small>391</small>                       | Input             | GPIO Header GPIO_G                            |                           |                          |                                                                                                                          |
| LS1088 GPIO4    | 8   | 616 <small>392</small>                       | Input             | GPIO Header GPIO_H                            |                           |                          |                                                                                                                          |
| LS1088 GPIO4    | 9   | 617 <small>393</small>                       | Input             | Interrupt from TCA9539                        |                           |                          |                                                                                                                          |
| LS1088 GPIO4    | 11  | 619 <small>395</small>                       | Output            | SFP+ Left Down (usually bound to lower port)  | LED off                   | LED on                   |                                                                                                                          |
| LS1088 GPIO4    | 12  | 620 <small>396</small>                       | Output            | SFP+ Right Up (usually bound to upper port)   | LED off                   | LED on                   |  
| LS1088 GPIO4    | 13  | 621 <small>397</small>                       | Input             | SD/SIM Tray Status                            | Tray Inserted/Present     | Tray ejected             |                                                                                                                          |

## Device Tree Paths for each controller

A [change](https://lore.kernel.org/lkml/cover.1662116601.git.christophe.leroy@csgroup.eu/T/) in Linux 6.2 meant that all the SoC GPIO numbers start at 512 with bus-attached (I2C or other) coming after.

| Controller   | DT Path                  | Default Linux GPIO Base  | Default Linux GPIO Base (before 6.2)
| -------------| ------------------------ | -------------------------|--------------------------|
| TCA9539      | /soc/i2c@2000000/gpio@76 | 640                      | 368                      |
| LS1088 GPIO1 | /soc/gpio@2300000        | 512                      | 480                      |
| LS1088 GPIO2 | /soc/gpio@2310000        | 544                      | 448                      |
| LS1088 GPIO3 | /soc/gpio@2320000        | 576                      | 416                      |
| LS1088 GPIO4 | /soc/gpio@2330000        | 608                      | 384                      |

The base number of LS1088 GPIO4 (384) being lesser than LS1088 GPIO3 (416) before Linux 6.2 is _not_ a typo.
