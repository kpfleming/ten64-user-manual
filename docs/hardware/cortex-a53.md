# Cortex-A53

Major features of the LS1088 implementation:

| Design Parameter               | Value        |
|--------------------------------|--------------|
| Clock Speed                    | Up to 1.6GHz |
| Manufacturing Process          | 28nm         |
| Revision                       | r0p4-00rel0  |
| ISA Version                    | ARMv8.0      |
| Cores            | 8 (two clusters of four)     |
| L1 ICache        | 32K with ECC protection   |
| L1 DCache        | 32K with ECC protection   |
| L2 Cache Size    | 2x1MB (1MB per per cluster) with ECC protection  |
| L2 data RAM input latency | 1 cycle |
| L2 data RAM output latency | 2 cycles |
| SCU L2 cache protection | Yes |
| SIMD (Neon)  | Yes |
| Double-Precision floating point | Yes |
| Cryptography extension | Yes |
| CPU cache protection | Yes |
| Execution | In-order |
| Virtualization | Yes |
| Cache protection | Yes |
| ARMv7 (32-bit backwards compatibility) | Yes |

*Note* Not all of these parameters (i.e cache sizes, latency) are the same across all Cortex-A53 implementations.

For example, an A53 cluster in the Raspberry Pi 3 has 512K of L2 cache vs 1024K on the LS1088.


The LS1088A also contains:

| Hardware Block                               | Version      |
|----------------------------------------------|--------------|
| GIC-500 Interrupt Controller (aka GIC-v3)    | r1p1-00eac0  |
| MMU-500 Memory Management Unit               | r2p4-00rel0  |
| TrustZone TZC-400                            |              |
| CCI-400 Cache Coherent Interconnect          |              |
