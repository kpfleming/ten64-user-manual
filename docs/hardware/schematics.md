# Ten64 Schematics

These are made available for the benefit of Ten64 owners interested in the underlying hardware.

## Ten64 Rev C
* [PDF Schematic](/hardware/schematics/ten64-rev-c-public-sch.pdf)
* [Component Placement Diagrams](/hardware/schematics/ten64-rev-c-component-placement.pdf)

For IP licensing and custom design enquiries, please contact sales@traverse.com.au

## Ten64 I/O Panel drawing

This is useful for those wishing to create their own enclosures for the Ten64.

* [Drawing of the Ten64 Desktop Case Panel](/hardware/schematics/ten64-front-panel-drawing.pdf)
