# Traverse Ten64 Platform Documentation

Applicable hardware: Ten64 Rev B / C (beta and production boards)

This documentation is still a work in progress, please [contact us](https://forum.traverse.com.au) with any feedback or
raise an [issue](https://gitlab.com/traversetech/ls1088firmware/ten64-user-manual/issues)
on the documentation [repository](https://gitlab.com/traversetech/ls1088firmware/ten64-user-manual).

## [Quickstart](quickstart)
Power, Serial console, Network interface names, distribution images

## [Does it run?](/applications/doesitrun)
Quick overview of common distributions and software known to work

### Pages of interest

#### Hardware
* [Hardware Compatibility List](hardware/hcl)
    * [DDR4 SO-DIMM](hardware/ddr) - supported SO-DIMM's.
* [Power Connector](hardware/power-connector)
* [GPIO list](hardware/gpio)
* [GPIO / Control Header](hardware/control-header)

#### Software
* [How to update firmware](software/firmwareupdate)
* [Feature roadmap](software/roadmap)
* [Supported distributions](software/distributions)
* [Recovery environment](software/recovery)

#### Kernel and Bootloader
* [Kernel configuration options and patches](kernel)
* [The boot process](software/bootprocess)
* [Firmware Builder](software/firmware-builder)

#### Networking
* [PHY Management modes (legacy/managed) explained](network/managementmodes)
* [DPAA2 Overview](network/dpaa2overview)
* [DPAA2 Setup and Configuration](network/dpaa2config)


#### Miscellaneous
* [Glossary](glossary)
* [Frequently Asked Questions](faq)

## Useful external links
### Firmware
* [Firmware releases](https://archive.traverse.com.au/) - Firmware builds and OpenWrt
* [Firmware source on Gitlab](https://gitlab.com/traversetech/ls1088firmware)

### Support
* [Traverse Support Forum](https://forum.traverse.com.au)
