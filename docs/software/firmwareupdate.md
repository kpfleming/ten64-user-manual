---
layout: page
title: Firmware Updating / Reflash
permalink: /software/firmwareupdate/
---

Updating the Ten64 board firmware can be done in several ways:

- From the [recovery](/software/recovery/) environment (**recommended**)

- From U-Boot, using the firmware-builder SD card image (if the onboard flash is unusable)

    The firmware-builder sdcard is configured to offer reflash as a menu option.

## From the recovery environment

1. Boot your Ten64 in recovery mode

    You can also use the firmware restore SD card and the "Boot Recovery" menu option.

    If you haven't used recovery mode before, remember to [setup WAN access](/software/recovery/#networking-in-recovery).

        set-wan ethX
    
    NOTE: By default, `eth0` is set as the 'LAN' interface (192.168.10.1/24). It is best to use a different Ethernet
    interface for "WAN".

2. Download the desired _Flash Tarball_ from [archive.traverse.com.au](https://archive.traverse.com.au)

    When downloading directly in the recovery environment, you should do so into `/tmp`

        cd /tmp
        wget https://archive.traverse.com.au/pub/traverse/ls1088firmware/firmware-builds/branches/v0.8-rtm/292153827/firmware-v0.8-rtm-292153827.tar.xz
        [snip]
        Download completed (51949688 bytes)

3. Extract the tarball:


        tar -Jxvf firmware-v0.8-rtm-292153827.tar.xz
        firmware-v0.8-rtm-292153827/
        firmware-v0.8-rtm-292153827/SHA256SUMS
        firmware-v0.8-rtm-292153827/flash.sh
        [snip]

4. Enter the firmware directory and run `./flash.sh`:

        ./flash.sh
        Unlocking bl2 ...
        Erasing bl2 ...
        Unlocking bl2 ...

        Writing from arm-trusted-firmware/build/ten64/debug/bl2_qspi.pbl to bl2 ...
        Unlocking bl3 ...
        Erasing bl3 ...
        Unlocking bl3 ...

        Writing from arm-trusted-firmware/build/ten64/debug/fip.bin to bl3 ...
        Unlocking mcfirmware ...
        Erasing mcfirmware ...
        Unlocking mcfirmware ...

        Writing from qoriq-mc-binary/ls1088a/mc_10.20.4_ls1088a.itb to mcfirmware ...
        Unlocking ubootenv ...
        Erasing ubootenv ...
        Unlocking dpl ...
        Erasing dpl ...
        Unlocking dpl ...

        Writing from dpaa2/dpl/eth-dpl-all.dtb to dpl ...
        Unlocking dpc ...
        Erasing dpc ...
        Unlocking dpc ...

        Writing from dpaa2/dpc/dpc.0x1D-0x0D.dtb to dpc ...
        Unlocking devicetree ...
        Erasing devicetree ...
        Unlocking devicetree ...

        Writing from dtb/fsl-ls1088a-ten64.dtb to devicetree ...
        ubiformat: mtd9 (nand), size 113246208 bytes (108.0 MiB), 864 eraseblocks of 131072 bytes (128.0 KiB), min. I/O size 2048 bytes
        libscan: scanning eraseblock 863 -- 100 % complete
        ubiformat: 864 eraseblocks have valid erase counter, mean value is 1
        ubiformat: flashing eraseblock 250 -- 100 % complete
        ubiformat: formatting eraseblock 863 -- 100 % complete

The onboard firmware has now been updated - you can reboot your Ten64.

## Using SD card
You can obtain a firmware-builder SD card image which allows the board to boot directly
from SD and perform the flashing operation within the U-Boot environment.

You will need to move the SW1-1 switch (closest to the board LEDs) to the 'OFF' position to boot the LS1088 from the microSD card.

![DIP switch and LEDs](/images/sw1-sd-boot-position.jpg)

Select _Reflash board_ from the menu presented.
![SD card boot menu](/images/sdcard_bootmenu.png)

It will execute a script that performs the flash sequence:
```
90817 bytes read in 23 ms (3.8 MiB/s)
Erasing 0x00000000 ... 0x000fffff (16 eraseblock(s))
Writing 1048576 byte(s) at offset 0x00000000
1114723 bytes read in 71 ms (15 MiB/s)
Erasing 0x00000000 ... 0x001fffff (32 eraseblock(s))
Writing 2097152 byte(s) at offset 0x00000000
937920 bytes read in 63 ms (14.2 MiB/s)
Erasing 0x00000000 ... 0x001fffff (32 eraseblock(s))
Writing 2097152 byte(s) at offset 0x00000000
10302 bytes read in 18 ms (558.6 KiB/s)
Erasing 0x00000000 ... 0x0003ffff (4 eraseblock(s))
Writing 262144 byte(s) at offset 0x00000000
562 bytes read in 17 ms (32.2 KiB/s)
Erasing 0x00000000 ... 0x0003ffff (4 eraseblock(s))
Writing 262144 byte(s) at offset 0x00000000
19765 bytes read in 19 ms (1015.6 KiB/s)
Erasing 0x00000000 ... 0x0003ffff (4 eraseblock(s))
Writing 262144 byte(s) at offset 0x00000000
```

This sequence is stored on the sdcard as ```spi_fw/flash-qspi.scr```, you can execute it manually with:
```
U-Boot=> load mmc 0 ${load_addr} spi_fw/flash-qspi.scr; source ${load_addr}
```

## Upgrading firmware components manually
### Using U-Boot
You can achieve the same result as the boot menu command above using the ```mtd erase```
and ```mtd write``` commands. You will need to load the binaries, either from mass storage
(using the ```load``` command) or network (with ```tftpboot```)

```
# Probe the NOR flash
sf probe 0:0
# Reset environment to default (to get correct mtdparts=)
env default -a
# Ensure the MTD subsystem parses the mtdparts=
mtd list
load mmc 0 $load_addr 'spi_fw/bl2_qspi.pbl'
mtd erase bl2
mtd write bl2 $load_addr +$filesize
load mmc 0 $load_addr 'spi_fw/fip.bin'
mtd erase bl3
mtd write bl3 $load_addr +$filesize
load mmc 0 $load_addr 'mcfirmware/mc_ls1088a.itb'
mtd erase mcfirmware
mtd write mcfirmware $load_addr +$filesize
load mmc 0 $load_addr 'dpaa2config/eth-dpl-all.dtb'
mtd erase dpl
mtd write dpl $load_addr +$filesize
load mmc 0 $load_addr 'dpaa2config/dpc.0x1D-0x0D.dtb'
mtd erase dpc
mtd write dpc $load_addr +$filesize
load mmc 0 $load_addr 'spi_fw/traverse-ten64-a.dtb'
mtd erase devicetree
mtd write devicetree $load_addr +$filesize
```

**NOTE: ** The U-Boot script currently (2021-06) does not write the recovery image and NAND-based OpenWrt
image, due to intermittent issues with the SPI-NAND writing in U-Boot.

You can update them in the recovery firmware with:
```
mtd erase recovery && mtd write recovery.itb recovery
UBIA_MTD_PART=$(cat /proc/mtd | grep ubia | awk -F ':' '{print $1}')
ubiformat "/dev/${UBIA_MTD_PART}" -f openwrt-nand.ubi
```

### From a Linux environment

!!! Note
    The SPI-NOR and MTD stack in kernel versions earlier than ~5.4 did not properly handle
    write operations to SPI-NOR using the newer fsl-quadspi driver.

You can also flash binaries from Linux using the ``mtd`` tool - this needs to be done
from a system that has access to the QSPI flash - such as [recovery](/software/recovery/) - the QSPI
controller is disabled for block device (e.g EFI) operating systems.


```
mtd erase bl2

mtd write bl2_qspi.pbl bl2
```

and so on..
