---
layout: page
title: Recovery Image
permalink: /recovery/
---

# Recovery System
The recovery system is a built-in ramdisk on the SPI-NAND containing useful tools
for writing operating system images to SSD's, chroot'ing into a existing install or
to update low level firmware. As the name might suggest, it's purpose is similar
to the recovery feature on Android phones.

The recovery system is based on OpenWrt, you can customize it for your own needs - e.g to perform automated image download.

You can also run the recovery image from other media sources such as USB or TFTP.

## Entering Recovery
There should be a recovery option in the bootmenu, otherwise, you can run:
```
run bootcmd_recovery
```
in U-Boot.

The recovery command is:
```
mtd read recovery $load_addr && sf read 0x80100000 0x580000 0x40000 && fsl_mc apply DPL 0x80100000 && bootm $load_addr#ten64
```

To run recovery from a different source, e.g TFTP, you can replace the mtd read command with:
```
tftpboot $load_addr recovery.itb
```

### Networking in recovery
By default, recovery will act as a DHCP server on 'eth0' on 192.168.10.1

To set up a DHCP WAN connection quickly, you can run ```set-wan ethX```, for example:
```
set-wan eth7
Bringing up eth7 as wan:
eth7      Link encap:Ethernet  HWaddr 00:0A:FA:24:24:16
          inet addr:192.168.0.117  Bcast:192.168.0.255  Mask:255.255.255.0
          inet6 addr: fe80::20a:faff:fe24:2416/64 Scope:Link
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:68 errors:0 dropped:0 overruns:0 frame:0
          TX packets:56 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000
          RX bytes:7330 (7.1 KiB)  TX bytes:6513 (6.3 KiB)
```
## Recovery Toolset
This is a partial list:

* Export a local disk over the network via nbd or iSCSI
    * nbd-server
    * tgtd
* Manipulate and/or copy images (qcow2 etc.)
    - write-image-disk will use qemu-nbd and qemu-img to do fast copies from image to disk
* Partitioning
    * parted
    * cfdisk
    * sfdisk
* LVM2 tools
    * {pv,vg,lv}create, display,etc
* SSD maintenance
    * blkdiscard
    * fstrim
* Filesystem operations
    * mkfs.{ext2,ext3,ext4}
    * resize2fs
* Manipulate flash devices
    * mtd
    * ubifs {ubiattach,ubiblock,ubidetech,ubiformat,ubimkvol,ubinfo etc.}
* Network
    * curl with TLS certificates
* Other general tools
    * dd
    * chroot

## Setting real time clock
As noted in [Quickstart](/quickstart#realtime-clock-rtc), the real time clock will only keep time offline for 48-72 hours, this means the board will likely have no time when you first receive it.

You can set the hardware RTC by:
```
# This will use the NTP servers defined in OpenWrt's UCI (usually x.openwrt.pool.ntp.org)
ntpd -q -n -p $(uci get system.ntp.server | awk '{print $1}')
# Set RTC time from system time
hwclock -w
```

You can check the real time clock status with:
```
hwclock
2020-07-29 06:19:39.394470+00:00
```

## Appliance Store
Using the [bare metal appliance store](/software/appliancestore/) you can download and write operating system images directly on the device without having to use any external media - all that is needed is an internet connection.

Appliance store commands:

* ``appstore-list``
* ``baremetal-deploy <appliance-id> </dev/_target_block_device_>``

## Example - downloading and writing a image to NVMe

```
# Download the image
root@recovery000afa242417:/tmp# curl https://archive.traverse.com.au/pub/traverse/ls1088firmware/distribution-images/debian/latest/debian.qcow2 -O
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  192M  100  192M    0     0  9344k      0  0:00:21  0:00:21 --:--:-- 11.0M

root@recovery000afa242417:/tmp# write-image-disk debian.qcow2 /dev/nvme0n1
About to write debian.qcow2 to /dev/nvme0n1

If you have not already, make sure you have unmounted all partitions and other
services such as LVM2 on /dev/nvme0n1
Starting in 10 seconds, hit Ctrl-C now to abort
[  562.432082]  nbd0: p1 p2 p3
388800+0 records in
388800+0 records out
15925[  574.647591] block nbd0: NBD_DISCONNECT
24800 bytes (1.6[  574.652189] block nbd0: Disconnected due to user request.
 GB, 1.5 GiB) co[  574.658947] block nbd0: shutting down sockets
pied, 12.2091 s, 130 MB/s
/dev/nbd0 disconnected
Warning: Not all of the space available to /dev/nvme0n1 appears to be used, you can fix the GPT to use all of the space (an extra 246959280 blocks) or continue with the current setting?
/dev/nvme0n1 written

# You can now reboot into your system
/sbin/reboot
```

## Distribution specific tools

### arch-setup
`arch-setup` can deploy [Arch Linux ARM](https://archlinuxarm.org) to your Ten64.

The setup steps are similar to those described in [Arch Linux ARM: Generic AArch64 Installation](https://archlinuxarm.org/platforms/armv8/generic).

It takes one argument: the disk device to deploy to.

```
# arch-setup /dev/nvme0n1
Downloading Arch Linux tarball
Making new GPT label
[  199.098210] EXT4-fs (nvme0n1p2): mounted filesystem with ordered data mode. Opts: (null)
[  273.597511] EXT4-fs (nvme0n1p2): mounted filesystem with ordered data mode. Opts: (null)
Executing: chroot /mnt/deployroot /bin/sh -l -c 'pacman-key --init'
Executing: chroot /mnt/deployroot /bin/sh -l -c 'pacman-key --populate archlinuxarm'
Executing: chroot /mnt/deployroot /bin/sh -l -c 'ln -s /proc/self/fd /dev/fd'
Executing: chroot /mnt/deployroot /bin/sh -l -c 'pacman --noconfirm -Syy'
Executing: chroot /mnt/deployroot /bin/sh -l -c 'pacman --noconfirm -R linux-aarch64'
Executing: chroot /mnt/deployroot /bin/sh -l -c 'pacman --noconfirm -S linux-aarch64-rc'
Executing: chroot /mnt/deployroot /bin/sh -l -c 'pacman --noconfirm -S grub dosfstools efibootmgr'
Executing: chroot /mnt/deployroot /bin/sh -l -c 'grub-install --removable --no-nvram /dev/vda'
Executing: chroot /mnt/deployroot /bin/sh -l -c 'ln -s /boot/Image /boot/vmlinuz-arch'
Executing: chroot /mnt/deployroot /bin/sh -l -c 'ln -s /boot/initramfs-linux.img /boot/initrd-arch.img'
Executing: chroot /mnt/deployroot /bin/sh -l -c 'grub-mkconfig -o /boot/grub/grub.cfg'
Arch Linux setup complete
```

As per the Arch Linux ARM tarball defaults, the root password is `root` - you can change it by
chrooting into `/mnt/deployroot` while still in recovery, if you wish.
 
### gentoo-setup

`gentoo-setup` can deploy [Gentoo](https://www.gentoo.org) or [Funtoo](https://www.funtoo.org/).

To ease any difficulties creating a working system, the `gentoo-setup` tool will download a
Traverse Debian kernel package .deb and extract it. If you wish to `emerge` your own kernel,
you may do so once you have a working system.

You need to download an appropriate stage3 tarball from [Gentoo](https://www.gentoo.org/downloads/)
or [Funtoo](https://www.funtoo.org/Arm64_generic) first, and then supply as an argument to
`gentoo-setup`:

```
cd /tmp
curl https://build.funtoo.org/1.4-release-std/arm-64bit/arm64_generic/2021-06-01/stage3-arm64_generic-1.4-release-std-2021-06-01.tar.xz -L -o funtoo-stage3.tar.xz
gentoo-setup /tmp/funtoo-stage3.tar.xz /dev/nvme0n1
Making new GPT label
Mounting /dev/nvme0n1p2 to deployroot
[  336.027403] EXT4-fs (/dev/nvme0n1): mounted filesystem with ordered data mode. Opts: (null)
Distribution is funtoo
[  407.437697] EXT4-fs (dev/nvme0n1): mounted filesystem with ordered data mode. Opts: (null)
Executing: chroot /mnt/deployroot /bin/sh -l -c 'ln -s /proc/self/fd /dev/fd'
Executing: chroot /mnt/deployroot /bin/sh -l -c 'ego sync'
Executing: chroot /mnt/deployroot /bin/sh -l -c 'emerge grub'
Getting package info from https://archive.traverse.com.au/pub/traverse/debian-experimental//dists/torvalds/main/binary-arm64/Packages
Installing these kernel packages
{
  ["https://archive.traverse.com.au/pub/traverse/debian-experimental/pool/main/l/linux-traverse/linux-image-5.13.0-traverse-torvalds+31-ge64b97ab0cca_5.13.0-traverse-torvalds+31-ge64b97ab0cca-1_arm64.deb"] = "3e8258f1fd90ccf5468713685e501f60e1295b847d489d9495c24c41ad25824f",
  ["https://archive.traverse.com.au/pub/traverse/debian-experimental/pool/main/t/traverse-sensors-dkms-bin/traverse-sensors-modules-5.13.0-traverse-torvalds+31-ge64b97ab0cca_0.01+714c43a_arm64.deb"] = "04e88937d53d48a6627b7f85ee35b3dd824acf6e16e661e54a8097d3b26f2ba8"
}
true
Executing: chroot /mnt/deployroot /bin/sh -l -c 'tar --exclude='\''./usr/*'\'' --exclude='\''./etc/*'\'' -C /  -Jhxf /tmp/traverse-sensors-modules-5.13.0-traverse-torvalds+31-ge64b97ab0cca_0.01+714c43a_arm64.deb_extract/data.tar.xz'
Executing: chroot /mnt/deployroot /bin/sh -l -c 'tar --exclude='\''./usr/*'\'' --exclude='\''./etc/*'\'' -C /  -Jhxf /tmp/linux-image-5.13.0-traverse-torvalds+31-ge64b97ab0cca_5.13.0-traverse-torvalds+31-ge64b97ab0cca-1_arm64.deb_extract/data.tar.xz'
Executing: chroot /mnt/deployroot /bin/sh -l -c 'grub-install --removable --no-nvram --efi-directory=/boot /dev/nvme0n1'
Executing: chroot /mnt/deployroot /bin/sh -l -c boot-update
Gentoo setup complete
Please chroot into /mnt/deployroot to set your root password
```

As noted, you should chroot into `/mnt/deployroot` and run `passwd` to set the root password:
```
root@recovery525400123456:/tmp# chroot /mnt/deployroot/
recovery525400123456 / # passwd root
New password:
Retype new password:
passwd: password updated successfully
recovery525400123456 / # exit
exit
root@recovery525400123456:/tmp#
```

## Updating the recovery image
Recovery images can be updated using ```mtd erase recovery && mtd write <file> recovery```

```
root@OpenWrt:/tmp# mtd erase recovery
Unlocking recovery ...
Erasing recovery ...
root@OpenWrt:/tmp# mtd write ten64-recovery-snapshot-r11175-962c7dae57-layerscape-armv8_64b-traverse-ls1088-initramfs-kernel.itb recovery
Unlocking recovery ...

Writing from ten64-recovery-snapshot-r11175-962c7dae57-layerscape-armv8_64b-traverse-ls1088-initramfs-kernel.itb to recovery ...
```
