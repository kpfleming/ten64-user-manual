# Docker on ARM64

Docker on ARM64 functions much the same way as on x86, however, a Docker host
can only run containers built for its architecture. Thankfully, multi-architecture
support has been built into recent docker versions.

Example:

```nohighlight
root@debian-arm64:~# uname -a
Linux debian-arm64 4.19.76 #2 SMP PREEMPT Tue Oct 15 01:03:53 UTC 2019 aarch64 GNU/Linux

root@debian-arm64:~# docker run -i -t debian:latest
Unable to find image 'debian:latest' locally
latest: Pulling from library/debian
627706d65d1d: Pull complete
Digest: sha256:f19be6b8095d6ea46f5345e2651eec4e5ee9e84fc83f3bc3b73587197853dc9e
Status: Downloaded newer image for debian:latest
root@b9bf9b807bb6:/# uname -a
Linux b9bf9b807bb6 4.19.76-dirty #2 SMP PREEMPT Tue Oct 15 01:03:53 UTC 2019 aarch64 GNU/Linux

```

It is also possible to run 32-bit ARM (``arm32v7``) Docker images on the LS1088 - as the Cortex-A53 is backwards
compatible:
```
docker run -i -t arm32v7/debian:latest
root@340a4dca86e3:/# bash --version
GNU bash, version 5.0.3(1)-release (arm-unknown-linux-gnueabihf)
Copyright (C) 2019 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>

This is free software; you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
```

.. however, be mindful that other ARM64 CPUs, especially those made by other vendors (Ampere, Marvell) do *not* have this backwards compatibility.

## Further reading:
- [Docker Official Images - Architectures other than amd64?](https://github.com/docker-library/official-images#architectures-other-than-amd64)
- [Docker Hub - arm64v8 images](https://hub.docker.com/u/arm64v8/)
- [linuxserver.io container library](https://hub.docker.com/u/linuxserver) - many of these are multi-arch, containers for UniFi,nextcloud and other interesting applications
