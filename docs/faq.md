# Highly Anticipated and/or Frequently Asked Questions

## Common issues
1. __Shortly after the kernel loads, the console turns to garbage__ (also __PCI Express devices fail to enumerate__)

    This occurs when the device tree passed to the kernel has not been 'fixed up'
    by U-Boot. On the LS1088 the fixup process sets up clocks, IOMMU and PCIe streams
    and other runtime properties. These are dependent on what peripherals are installed,
    so cannot be statically set in the device tree.

    In our U-Boot configuration this is performed by the ``setup_distroboot_efi`` command before the main
    boot sequence:

        setup_distroboot_efi=sf read 0x80001000 0x580000 0x40000 &&
          fsl_mc lazyapply dpl 0x80001000;  sf read $fdt_addr_r 0x600000 0x40000 &&
          fdt addr $fdt_addr_r &&
          fdt resize 10000 &&
          fdt rm /soc/spi &&
          fdt boardsetup
        bootcmd=run setup_distroboot_efi; run distro_bootcmd

    Due to issues with stream ID's not being set correctly on successive fixup invocations, the
    fixup handler for the Ten64 board will only apply fixups once (e.g when 'fdt boardsetup' is called).

    If the device tree passed to the kernel is not the same as the fixed copy, the same situation will arise.

2. __When booting something using Kernel 5.1 or later the boot fails with IOMMU fault messages__

    Add the ```arm-smmu.disable_bypass=n``` parameter to the kernel command line. This is a
    temporary fix and hopefully the need to do this will disappear in the near future.

    See the [Distributions](/software/distributions/) and [Kernel](/kernel) page for more information.

## Boot
1. __How do I change the default boot order in U-Boot__ (booting from USB or SD instead of NVMe etc.)

      Change the ``boot_targets`` environment variable in U-Boot:

        => print boot_targets
        boot_targets=nvme0 mmc0 usb0 openwrt_nand backstop scsi0
        => setenv boot_targets 'usb0 nvme0 mmc0 openwrt_nand backstop scsi0'
        => saveenv
        Saving Environment to SPI Flash... Erasing SPI flash...Writing to SPI flash...done
        OK

    If you wish to reset the order to the default, you can use 'env default boot_targets && saveenv' to do so.

2. __How can I use a U-Boot bootscript in front of/instead of EFI?__

    Due to the IOMMU issue mentioned above, if you want to run a U-Boot script for boot, your
    script __must result in a kernel or EFI handoff__. If your script returns back to U-Boot,
    you will experience issues with the IOMMU workaround.

    You can hand off directly to EFI in a U-Boot script by ending with:

        echo "Booting from ${devtype} ${devnum}:${distro_bootpart}"
        load ${devtype} ${devnum}:${distro_bootpart} ${kernel_addr_r} efi/boot/bootaa64.efi bootefi ${kernel_addr_r} ${fdt_addr_r}

    Note that this behaviour may change in future firmware/U-Boot versions.

## Software
1. __Does FreeBSD (and related distributions, pfsense, FreeNAS etc) run on the Ten64?__

    It does now! Thanks to the work of the FreeBSD community. See [FreeBSD preview for Ten64](https://forum.traverse.com.au/t/freebsd-preview-for-ten64/173/4) for more information.

2. __Are there any 'binary blobs' needed for hardware on the Ten64?__

    Yes, the [DPAA2](/network/dpaa2overview/) hardware (Ethernet ports and some crypto acceleration functions)
    require the "management complex" firmware to be loaded. This runs on a microcontroller code inside the LS1088 - not
    the Cortex-A53's.

    The management complex firmware is loaded by U-Boot, just before hitting the command prompt (or default `bootcmd`). We
    do NOT expect distributions to ship this as part of their firmware repository.

3. __How do I compile/add extra packages to OpenWrt and derived distributions?__ (without compiling OpenWrt from scratch)

    The best way is to use the [OpenWrt SDK](https://openwrt.org/docs/guide-developer/toolchain/using_the_sdk). You
    can find the SDK package for our OpenWrt builds on our archive server, usually named
    `openwrt-sdk-arm64-efi_gcc-8.4.0_musl.Linux-x86_64.tar.xz` or similar.

    If you think this package is useful for others as well, feel free to post a suggestion on our forum.

    You can also use the __ImageBuilder__ to roll images with your own package combinations (both pre-built
    from our repository and your own ipk's).

    See these pages for more information:

    * muvirt wiki: [Using SDK and ImageBuilder](https://gitlab.com/traversetech/muvirt/-/wikis/Using-SDK-and-ImageBuilder)
    * Forum post: [wireguard's qrencode](https://forum.traverse.com.au/t/wireguards-qrencode/60/2)

## Power and Mechanical
1. __What is the typical power consumption for a Ten64 unit?__

    Typical consumption (measured with a lab power supply) is between 14-18W with RAM and SSD installed.

    RAM modules with many chips (16,32GB), wireless modules/modems and USB devices
    can increase the power draw, but we haven't seen more than 30W drawn in practice.

    The amperage rating (5A) on the Ten64 compliance label is reflective of the _maximum_
    draw the Ten64 power system has been designed to provide in a 'maxed-out' sitation
    with high power WiFi and Cellular cards installed.

    Traverse typically bundles a 50-60W power supply - the increased wattage is to ensure
    longevity and reliability of the power supply.

2. __Can Ten64 be rack mounted?__

    Ten64 has been designed with rackmounting in mind. The enclosure is exactly 1U high, but
    a conversion kit has not been manufactured yet.

    Please contact us for more details.

3. __What are the DC power supply requirements?__

    Ten64 uses a 12V DC external power supply unit, with 2.5mm barrel, center-positive.

    The connector needs to be around 12mm long. Some other 12V DC, 2.5mm supplies on the market
    have shorter connectors which will not work.

    __For electrical safety, you must use a three-prong power supply where the Earth pin
    is connected to GND.__