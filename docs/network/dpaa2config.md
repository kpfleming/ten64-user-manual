# DPAA2 Ethernet configuration

The DPAA2 network complex is somewhat unique as the resources available are not
"hardwired" - they can be connected dynamically to achieve various scenarios, such
as connecting certain ports to virtual machines or to accelerators such as the AIOP.

DPAA2 can be complicated to understand - what is presented here is an attempt to simplify
it for those looking to use it with standard Linux network interfaces.

For a more in depth overview of DPAA2 technologies, see [DPAA2 overview](/network/dpaa2overview)
## Terms you need to know:
* _MC_: _Management Complex_

    This is the internal engine in the DPAA2 complex which manages the DPAA2 objects, and abstracts
    many of the low level details away from the operating system.

* _DPC_: Data path configuration file

    The data path configuration file sets out the hardware resources available - most users do
not need to touch this file.

* _DPL_: Data path layout file

    The DPL sets up the connections between resources in the DPAA2 complex - practically this is where
MACs are connected to the network controllers.

* _DPNI_: Data path network interface

    This is most analogous to an "eth" interface in Linux

* _DPMAC_: Data path MAC:

    This is the "front" end of an Ethernet interface - what connects to the PHY.

* _DPIO_: Data path I/O

    This is the operating system interface to the data plane - all frames are transmitted and
received through a DPIO object. The LS1088 has eight DPIO objects available so the processing
of frames can be spread across all cores.

The simplest configuration is to connect each DPNI to a DPMAC.

Other end point for DPNI's exist such as DPDMUX or DPSW in the larger SoCs (LS/LX2XXX) - for more information on these, please see the NXP documentation.

## The default configuration (eth-dpl-all)
| MAC name | Device      | Name on Panel | Linux Interface Name (10 port config) |
|----------|-------------|---------------|---------------------------------------|
| DPMAC7   | VSC8514 U21 | GE0           | eth0                                  |
| DPMAC8   | VSC8514 U21 | GE1           | eth1                                  |
| DPMAC9   | VSC8514 U21 | GE2           | eth2                                  |
| DPMAC10  | VSC8514 U21 | GE3           | eth3                                  |
| DPMAC3   | VSC8514 U29 | GE4           | eth4                                  |
| DPMAC4   | VSC8514 U29 | GE5           | eth5                                  |
| DPMAC5   | VSC8514 U29 | GE6           | eth6                                  |
| DPMAC6   | VSC8514 U29 | GE7           | eth7                                  |
| DPMAC1   | SFP+        | XG0           | eth8                                  |
| DPMAC2   | SFP+        | XG1           | eth9                                  |

In U-Boot, these will be prefixed based on the interconnect type, e.g 'DPMAC6@qsgmii' for VSC8514 and
'DPMAC1@xgmii' for SFP+

The default DPC and DPL configuration is provided in the firmware builder repository.

Ten64 ships with a configuration that makes all 10 ethernet ports available. However,
this is a compromise configuration that stretches the resources in the LS1088 - if you
don't need all 10 interfaces it is a good idea to remove any you are not using.

(Up to nine interfaces can be supported without compromising the internal resources in the LS1088)

## Boot time set up and deployment
To get working network interfaces the following steps need to happen, at the bootloader stage:

1. Load the MC firmware and DPC file into memory and then start the management complex
For example (when U-Boot is loaded from QSPI):
```
mcinitcmd=sf probe 0:0;sf read 0x80000000 0x300000 0x200000;sf read 0x80100000 0x5C0000 0x40000;fsl_mc start mc 0x80000000 0x80100000
```

2. Read the DPL file and (lazy)apply it:
```
sf read 0x80001000 0x580000 0x40000 && fsl_mc lazyapply dpl 0x80001000
```

Lazyapply delays the application of the DPL until the hand off to Linux, so you can still use the network interfaes in U-Boot.

## Runtime configuration
You can also leave DPNI's and DPMAC's unconfigured in the DPL and connect them at runtime using restool.

## Resource object dependencies
Each network interface (DPNI) requires a certain number of dependent objects (such as DPMCPs or DPCONs).

A full overview can be found in NXP's [DPAA2 objects dependencies](https://docs.nxp.com/bundle/GUID-487B2E69-BB19-42CB-AC38-7EF18C0FE3AE/page/GUID-C254A494-0853-4C94-8AD0-7D9C2739B5D3.html#GUID-C254A494-0853-4C94-8AD0-7D9C2739B5D3), which can be summarized as:

* Each DPNI requires a DPBP and DPMCP, plus N * DPCON where `N` is the number of receive queues.
* Each DPMAC being controlled by an operating system requires one DPMCP
* One DPIO is required per CPU core, and each DPIO requires a DPMCP.
 
## Memory reservation
DPAA2 requires a portion of host memory to function, on the Ten64 we use the lowest amount by default (around ~700MiB). If you use AIOP or push high amounts of packets, you
may need to adjust this using the "mcmemsize" variable in U-Boot. A reboot is required for changes to "mcmemsize" to take effect.

See [README.lsch3](https://gitlab.denx.de/u-boot/u-boot/blob/master/arch/arm/cpu/armv8/fsl-layerscape/doc/README.lsch3) in U-Boot for more details on the MC memory reservation.

As a guide, NXP sets a default of "mcmemsize=0x70000000" (1.75GiB) on the developer boards.

## Useful references
* [Layerscape SDK User Guide](https://www.nxp.com/docs/en/user-guide/LSDKUG_Rev19.09.pdf) chapter 8.3 (DPAA2-specific Software)
Embedded in the SDK user guide is the following (use the 'Attachments' tab in Acrobat Reader to access):
    * DPAA2 user manual
    * DPAA2 API reference
    * AIOP User manual
    * AIOP Service Layer reference

* [QorIQ LS2 Series DPAA2 training presentation](https://community.nxp.com/docs/DOC-330319)

* [QorIQ MC Firmware releases and changelog](https://github.com/NXP/qoriq-mc-binary)
