# Cellular Connectivity

Ten64 boards have a M.2 Key B slot with SIM designed for use
with LTE and 5G modems. Both USB3 and PCIe are supported, but the vast majority
of LTE cards use USB only for host connectivity.

A list of recommended modems can be found in the [Hardware Compatibility List](/hardware/hcl).

We recommend ModemManager for managing the LTE connection, especially since some
newer models do not expose full featured AT ports, meaning all communication has to be
done through a single userspace process.

# Antennas for 5G
As both WiFi and cellular technologies increase the number of MIMO streams, it is getting difficult to mount large numbers of antennas inside the one box. To complicate this, 4G and 5G run on a vast array of bands, simultaneously using carrier aggregation.

For 5G we recommend using a 4-in-1 (or higher) external MIMO antenna - such as the [Taoglas MA963](https://www.taoglas.com/product/guardian-ma963-4-in-1-4lte-mimo-adhesive-mount-antenna/). The rear panel of the Ten64 has been designed to interface with such arrays, while the sides are best used for WiFi antennas.

## Using Qualcomm PCI Express (MHI bus) modems

The MHI (modem host interface) bus is used on recent Qualcomm 4G and 5G modem 
chipsets, like the SDX55. MHI is designed to work both "inside" a SoC (e.g 
smartphones where the CPU and modem is inside the same chip) and "outside" 
(over PCI Express).

Because of this, PCIe/MHI modems need a bit of special treatment. Some modems
are "flashless" and require the host to supply the firmware.

We have a [repository](https://gitlab.com/traversetech/qcom-5g-mhi-support) of
scripts that can be used to setup these modems correctly, as well as working
around quirks that have not been fixed in the mainline kernel.

# Cellular Control GPIOs
M.2 modems have a couple of control lines:

|	 Name				        | 	Linux GPIO number	|	Effect of high value (1) | Effect of low value (0) |
|---------------------|---------------------|--------------------------|--------------------------|
|Full_Card_Power_Off#	|		377				        |	Card powers down	 | Card is active |
|RESET#					|	    376				|   Card performs hard reset | Card is active |
|W_DISABLE#				|378				|	Card enters 'airplane mode' | Modem RF is active |
|GNSS_DISABLE#    |379     | GNSS (Navigation) receiver disabled | GNSS receiver enabled |

See the [GPIO page](/hardware/gpio) for the full GPIO list and driver details.

__Note__: All these lines are connected to the modem via transistors, which means a high value (1) from the host will
become a low value (0) to the modem.

## SIM switching
Use of two SIM cards is possible with modems that have the second set of SIM pins on
their M.2 connector - this scheme has been implemented by most Qualcomm based Cat6 and above cards.

There is no SIM switching facility on the Ten64 board.

We also provide a build time option to install an embedded/soldered SIM instead of SIM2 which is useful for providing fallback to 'global'/roaming SIMs.

## Additional control signals
Some high-bandwidth cards have antenna control ("ANTCTRL") GPIOs that can be used to select a specific antenna configuration
(e.g when antennas are connected via a switch IC) based on factors such as the current bands in use. These I/O's are exposed on
the underside of the PCB as P15.

## Compliance note
When LTE and WiFi cards are installed in the same chassis, the use of co-existence filters
on the WiFi antennas is recommended.

Depending on the regulatory and telco rules for the region the product is being
sold in, additional certification testing may be required before the product can
be sold to end users.
